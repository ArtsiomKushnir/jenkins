# Variables
SHELL := /bin/bash

DOCKER_REPO := akushnir
NAME := jenkins
TAG  := 1.0

IMAGE := ${DOCKER_REPO}/${NAME}:${TAG}
USER_ID ?= $(shell stat -c "%u:%g" .)

DOCKER ?= docker
# DOCKER_COMPOSE_OPTS ?= --project-name ${NAME}
DOCKER_COMPOSE ?= IMAGE=${IMAGE} USER_ID=${USER_ID} docker-compose

# Tasks
run:
	${DOCKER_COMPOSE} -f ./docker/docker-compose.yml up --force-recreate jenkins

build:
	${DOCKER} build -f ./docker/Dockerfile -t ${IMAGE} .

push: build
	${DOCKER} push ${IMAGE}

clean: DOCKER_COMPOSE_FILES := $(sort $(wildcard ./docker/docker-compose*.yml))
clean: DOCKER_COMPOSE_FILES := $(patsubst %.yml,-f %.yml, ${DOCKER_COMPOSE_FILES})
clean:
	${DOCKER_COMPOSE} ${DOCKER_COMPOSE_FILES} stop
	${DOCKER_COMPOSE} ${DOCKER_COMPOSE_FILES} rm --force -v
	@-${DOCKER} rmi ${IMAGE}